package vid14;

import java.util.LinkedHashSet;
import java.util.Set;


import vid13.Persona;

public class AppTreeSet {

	public static void main(String[] args) {
		Set<Persona> lista = new LinkedHashSet<>();
		lista.add(new Persona(1,"MitoCode",22));
		lista.add(new Persona(2,"Mito",23));
		lista.add(new Persona(3,"Code",24));
		lista.add(new Persona(3,"Code",24 ));
		lista.add(new Persona(5,"Ruben",26));
		lista.add(new Persona(6,"AAA",27));
		
		for(Persona per:lista){
			System.out.println(per.getId() +"-"+per.getEdad()+"-"+per.getNombre());
		}
	}

}
