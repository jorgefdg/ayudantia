package vid5;

public class Conexion {
	private static Conexion instancia = null;
	
	public static Conexion getInstance(){
		if (instancia == null){
			instancia = new Conexion();
		}
		return instancia;
	}
}
