package vid34;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class AppNIO2 {

	public void operacionesFile(String operacion) throws IOException {

		Path pathOrigen = Paths.get("doge.txt");
		Path pathDestino = Paths.get("dogeFolder/doge.txt");

		switch (operacion) {
		case "existe":
			// boolean existe = Files.exists(path, new LinkOption[]{
			// LinkOption.NOFOLLOW_LINKS});
			// System.out.println(existe);
			break;
		case "crear":
			// Path nuevoPath = Files.createDirectory(path);
			break;
		case "copiar":
			Files.copy(pathOrigen, pathDestino, StandardCopyOption.REPLACE_EXISTING);
			break;
		case "mover":
			Files.move(pathOrigen, pathDestino, StandardCopyOption.REPLACE_EXISTING);
			break;
		case "eliminar":
			Files.delete(pathDestino);
			break;
		}

	}

	public void leer(){
	}

	public void escribir(){
	}

	
	public static void main(String[] args) throws IOException {
		AppNIO2 app = new AppNIO2();
		app.operacionesFile("copiar");

	}

}