package vid24;

import java.io.IOException;

public class AppExc {

	public static void main(String[] args) {
		AppExc app = new AppExc();
		app.mostrar();
	}
	
	public void mostrar() {
		try {
			throw new IOException("IOException");
		} catch (NullPointerException | IOException | NumberFormatException ex) {
			System.out.println(ex.getMessage());
		}
}
}
