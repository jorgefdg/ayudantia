package vid4;

import java.util.ArrayList;
import java.util.List;



public class App {
	private static List canasta = new ArrayList();

	public static void main(String[] args) {
		/*String texto=new String("Mitocode");
		if(texto instanceof String){
			System.out.println("Es un String");
		}*/
		
		/*Alumno al = new Alumno();
		if (al instanceof Persona){
			System.out.println("Es una persona");
		}*/
		
		System.out.println("Canastaa abierta, favor ingresar solo frutas");
		
		
		Manzana m1= new Manzana("roja");
		Manzana m2= new Manzana("verde");
		Naranja n1=new Naranja("naranjita");
		Galleta g1 = new Galleta("chocolate");
		
		canasta.add(m1);
		canasta.add(m2);
		canasta.add(n1);
		canasta.add(g1);
		
		App app = new App();
		app.verificar(m1);
		app.verificar(m2);
		app.verificar(n1);
		app.verificar(g1);
	}

	private void verificar(Object objeto){
		if(objeto instanceof Fruta){
			canasta.add(objeto);
			System.out.println("Fruta agregada "+((Fruta)objeto).getNombre());
		}else{
			System.out.println("Elemento no permitido, solo frutas por favor");
		}
	}
	
}
