/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vid12;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author lab
 */
public class App {
    
   public static void main(String[] args) {
       
       //int[] arreglo = new int [3];
       
       List<Persona> lista = new ArrayList<>();
       lista.add(new Persona(1, "Jaime", 25));
       lista.add(new Persona(2, "Alonso", 22));
       lista.add(new Persona(3, "Tobias", 17));
       
       Collections.sort(lista);
       
       for (Persona p : lista){
                  System.out.println(p.getEdad() + p.getNombre());
       }

   }    
    
}
