package vid27;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

public class AppStatement {

	private Connection con = null;

	public void conectar() {
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con = null;
			con= DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "root");
			System.out.println("conectado");
		}catch(Exception e){
			System.out.println("conexion fallida");
		}
	}
	
	public void desconectar() throws SQLException {
		if (con != null) {
			con.close();
		}
	}
	
	public boolean leerStatement(Persona per) throws SQLException {
		boolean rpta=false;
		try(Statement st = con.createStatement()){
			String sql = "Select * from personas WHERE nombre ='"+per.getNombre() +"' and pass = '"+ per.getPass() +"'";
			System.out.println("Query: "+sql);
			ResultSet rs = st.executeQuery(sql);
			
			if(rs.next()){
				System.out.println("existen datos");
				rpta=true;
			}else{
				System.out.println("no existen datos");
			}
			
			System.out.println("Consulta exitosa");
		}
		return rpta;
	}

	

	public void registrarCallableStatement(Persona per){
		try{
			String sql = "{call spTest(?,?)}";
			CallableStatement cs = con.prepareCall(sql);
			cs.setString(1, per.getNombre());
			cs.setString(2, per.getPass());
			cs.execute();
			cs.close();
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
	
	public void listarCallableStatement(){
		try{
			String sql = "{call spListar()}";
			CallableStatement cs = con.prepareCall(sql);			
			cs.execute();
			
			ResultSet rs = cs.getResultSet();
			
			while(rs.next()){
				System.out.print(rs.getInt("id"));
				System.out.print(rs.getString("nombre"));
				System.out.println(rs.getString("pass"));
			}
			cs.close();
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
	
	public void listarOutCallableStatement(Persona per){
		try{
			String sql = "{call spSalidaID(?,?)}";
			CallableStatement cs = con.prepareCall(sql);			
			cs.setString(1, per.getNombre());
			cs.registerOutParameter("idParam", Types.INTEGER);
			cs.execute();
			
			int idSalida = cs.getInt("idParam");
			System.out.println("El codigo obtenido de salida es: " + idSalida);
			cs.close();		AppStatement app = new AppStatement();
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
}

	public static void main(String[] args) throws SQLException {
		AppStatement app = new AppStatement();
		app.conectar();

		//app.registrarCallableStatement(new Persona("Ringo", "lepass"));
		app.listarOutCallableStatement(new Persona("Ringo", "lepass"));

		app.desconectar();
	}

}