package vid23;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MatcherApp {

	public static void main(String[] args) {
		String texto = "no se que escribir aqui no no";
		Pattern p = Pattern.compile("no", Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(texto);
		
		System.out.println("matchers "+ m.matches());
		System.out.println("lookingAt "+ m.lookingAt());
		
		int cont =0;
		while(m.find()){
			cont++;
			System.out.println("Coincidencia N°"+cont+" start "+m.start()+" end "+m.end());
		}
	}

}
