package vid25;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

public class AppStatement {

	private Connection con = null;

	public void conectar() {
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con = null;
			con= DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "root");
			System.out.println("conectado");
		}catch(Exception e){
			System.out.println("conexion fallida");
		}
	}
	
	public void desconectar() throws SQLException {
		if (con != null) {
			con.close();
		}
	}
	
	public boolean leerPreparedStatement(Persona per) throws SQLException {
		boolean rpta=false;
		PreparedStatement ps = null;
		try(Statement st = con.createStatement()){
			String sql = "Select * from personas WHERE nombre = ? and pass = ?";
			System.out.println("Query: "+sql);
			
			ps=con.prepareStatement(sql);
			ps.setString(1,per.getNombre());
			ps.setString(2,per.getPass());
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()){
				System.out.println("existen datos");
				rpta=true;
			}else{
				System.out.println("no existen datos");
			}
			
			System.out.println("Consulta exitosa");
		}
		return rpta;
	}
	
	public void modificarBatchStatement(Persona per) throws SQLException {
		long ini = System.currentTimeMillis();
		try {
			con.setAutoCommit(false);

			for (int i = 0; i < 10000; i++) {
				Statement st = con.createStatement();
				String sql = "UPDATE personas SET nombre = '" + per.getNombre() + "', pass = '" + per.getPass() + "'";
				//System.out.println("Query => " + sql);
				int numeroFilas = st.executeUpdate(sql);
				//System.out.println("#Filas Afectadas - Statement " + numeroFilas);
			}
			con.commit();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			con.rollback();
		}
		long fin = System.currentTimeMillis();

		System.out.println("Statement : " + (fin - ini));
	}
	
	public void modificarBatchPreparedStatement(Persona per) throws SQLException {
		long ini = System.currentTimeMillis();
		try {
			con.setAutoCommit(false);
			PreparedStatement ps = null;
			for (int i = 0; i < 10000; i++) {
				String sql = "UPDATE personas SET nombre = ?,pass = ?";
				ps = con.prepareStatement(sql);
				ps.setString(1, per.getNombre());
				ps.setString(2, per.getPass());
				ps.addBatch();
			}
			ps.executeBatch();
			con.commit();
		} catch (Exception e) {
			con.rollback();
			System.out.println(e.getMessage());
		}
		long fin = System.currentTimeMillis();

		System.out.println("PreparedStatement : " + (fin - ini));
	}


	public static void main(String[] args) throws SQLException {
		AppStatement app = new AppStatement();
		app.conectar();
		app.modificarBatchPreparedStatement(new Persona("Shiro", "pass"));
		System.out.println("-------------------");
		app.modificarBatchStatement(new Persona("Shiro", "pass"));
		app.desconectar();
		

	}

}