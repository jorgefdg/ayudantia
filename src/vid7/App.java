package vid7;

import java.util.List;
import java.util.ArrayList;

public class App {

	public static void main(String[] args) {
		/*List<String> lista = new ArrayList<>();
		lista.add("le String");
		//lista.add(25);
		
		String texto=lista.get(0);
		System.out.println(texto);
		
		
		/*
		String[] array = new String[5];
		array[0]="String";
		array[1]=1;*/
		
		Clase <String,Integer,String,Double> c = new Clase <>("String", 25, "String2", 25.0);
		//c.mostrarTipo();
		
		List<Clase <String,Integer,String,Double>> lista = new ArrayList<>();
		lista.add(new Clase <String,Integer,String,Double>("String", 25, "String2", 25.0));
		
		for (Clase <String,Integer,String,Double> c1 : lista){
			c1.mostrarTipo();
		}
	}
}
