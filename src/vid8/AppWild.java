package vid8;

import java.util.ArrayList;
import java.util.List;

public class AppWild {
	
	public void listarUpperBounded(List<? extends Persona> lista){
		for (Persona per: lista){
			//if (a instanceof Alumno){
				System.out.println(per.getNombre());
			//}else if(a instanceof Profesor){
				//System.out.println(((Profesor) per).getNombre());
			//}
		}
	}
        
        public void listarLowerBounded(List<? super Alumno> lista){
		for (Object al: lista){
			//if (a instanceof Alumno){
				System.out.println(((Alumno) al).getNombre());
			//}else if(a instanceof Profesor){
				//System.out.println(((Profesor) añ).getNombre());
			//}
		}
	}
        
        public void listarUnBounded(List<?> lista){
		for (Object al: lista){
			//if (a instanceof Alumno){
				System.out.println(((Persona) al).getNombre());
			//}else if(a instanceof Profesor){
				//System.out.println(((Profesor) añ).getNombre());
			//}
		}
	}
        
        
	
	public static void main(String[] args) {
		AppWild aw = new AppWild();
		
		Persona a11 = new Alumno("Alberto");
		Persona a12 = new Alumno("Alfonso");
		Persona a13 = new Profesor("Tobias");
		
		List<Persona> lista = new ArrayList<>();
		lista.add(a11);
		lista.add(a12);
		lista.add(a13);
		
		aw.listarUpperBounded(lista);
                aw.listarLowerBounded(lista);
                aw.listarUnBounded(lista);
	}

}
