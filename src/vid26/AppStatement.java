package vid26;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

public class AppStatement {

	private Connection con = null;

	public void conectar() {
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con = null;
			con= DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "root");
			System.out.println("conectado");
		}catch(Exception e){
			System.out.println("conexion fallida");
		}
	}
	
	public void desconectar() throws SQLException {
		if (con != null) {
			con.close();
		}
	}
	
	public boolean leerStatement(Persona per) throws SQLException {
		boolean rpta=false;
		try(Statement st = con.createStatement()){
			String sql = "Select * from personas WHERE nombre ='"+per.getNombre() +"' and pass = '"+ per.getPass() +"'";
			System.out.println("Query: "+sql);
			ResultSet rs = st.executeQuery(sql);
			
			if(rs.next()){
				System.out.println("existen datos");
				rpta=true;
			}else{
				System.out.println("no existen datos");
			}
			
			System.out.println("Consulta exitosa");
		}
		return rpta;
	}


	public static void main(String[] args) throws SQLException {
		AppStatement app = new AppStatement();
		app.conectar();
		boolean resp = app.leerStatement(new Persona("Shi","pass' OR 'M'='M"));
		app.desconectar();
		
		if(resp){
			System.out.println("verificacion correcta");
		}else{
			System.out.println("acceso denegado");
		}
	}

}