package vid15;

import java.util.Set;
import java.util.TreeSet;

import vid13.Persona;

public class AppLinkedHashSet {

	public static void main(String[] args) {
		Set<Persona> lista = new TreeSet<>();
		lista.add(new Persona(1,"MitoCode",22));
		lista.add(new Persona(2,"Mito",23));
		lista.add(new Persona(3,"Code",24));
		lista.add(new Persona(3,"Code",24 ));
		lista.add(new Persona(5,"Ruben",26));
		lista.add(new Persona(6,"AAA",27));
		
		for(Persona per:lista){
			System.out.println(per.getId() +"-"+per.getEdad()+"-"+per.getNombre());
		}
	}

}
