package vid29;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;


public class InputReaderApp {

	public void leerPorFileInputStream() throws IOException {

		//bytes[]
		long ini = System.currentTimeMillis();
		try (InputStream fis = new FileInputStream("doge.jpeg")) {

			byte[] arreglo = new byte[1024];
			int i = fis.read(arreglo);

			while (i != -1) {
				//System.out.println("bytes leidos " + i);
				i = fis.read(arreglo);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		long fin = System.currentTimeMillis();
		System.out.println("Tiempo InputStream bytes[] " + (fin - ini));
		
		//byte a byte
		ini = System.currentTimeMillis();
		try (InputStream fis = new FileInputStream("doge.jpeg")) {
			
			int i = fis.read();

			while (i != -1) {
				//System.out.println("bytes leidos " + i);
				i = fis.read();
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		fin = System.currentTimeMillis();
		System.out.println("Tiempo InputStream byte " + (fin - ini));
	}
	

	public static void main(String[] args) throws IOException {
		InputReaderApp app = new InputReaderApp();
		app.leerPorFileInputStream();
	}
}
