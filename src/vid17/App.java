package vid17;

import java.util.Stack;

public class App {

	public static void main(String[] args) throws InterruptedException {
		Stack<Persona> pila = new Stack<Persona>();
		pila.push(new Persona(4, "Mitocode", 25));
		pila.push(new Persona(3, "Mitocode", 25));
		pila.push(new Persona(2, "Mitocode", 25));
		pila.push(new Persona(1, "Mitocode", 25));
		
		for(Persona element : pila){
			System.out.println(element);
		}
		System.out.println("Tope: "+pila.peek());
		System.out.println(pila.search(new Persona(1,"Mitocode",25)));
		
		System.out.println("LIFO");
		while(!pila.isEmpty()){
			System.out.println("Atendiendo a "+pila.pop());
			Thread.sleep(1000);
		}
	}

}
